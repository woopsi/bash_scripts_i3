#!/bin/bash

layout=$(xset -q | grep -A 0 'LED' | cut -c59-67);
capslock_state=$(xset q | grep Caps | awk '{print $4}');

if [[ $layout == "00000000" ]]; then
	echo US 
else
	echo GR 
fi


