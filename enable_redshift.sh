#!/bin/bash

is_redshift_on=$(pgrep redshift);

if [-z $is_redshift_on]; then
	redshift -l 40.73:20.92
else
	killall redshift
	redshift -l 40.73:20.92
fi

