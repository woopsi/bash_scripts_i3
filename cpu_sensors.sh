#!/bin/bash

sensor1=$(sensors coretemp-isa-0000 | awk 'NR == 4 {print $3}')
sensor2=$(sensors coretemp-isa-0000 | awk 'NR == 5 {print $3}')

echo Core1: $sensor1 Core2: $sensor2
