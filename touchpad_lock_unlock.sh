#!/bin/bash

value=$(xinput list-props 12 | grep "Device Enabled" | awk '{print $NF}');

if [[ $value == "1" ]]; then
	xinput set-prop 12 145 0
else
	xinput set-prop 12 145 1
fi
