#!/bin/bash
close_message="Press esc to close the dialog";
poweroff="Poweroff: ";
reboot="Reboot: ";
out="Logout: ";
cancel="Cancel: ";
action=$(echo -e "$poweroff\n$reboot\n$out\n$cancel" | dmenu -fn 'Font Awesome 8' );

if [[ ${action,,} == "${poweroff,,}" ]]; then
	systemctl poweroff;
elif [[ ${action,,} == "${reboot,,}" ]]; then
	systemctl reboot;
elif [[ ${action,,} == "${out,,}" ]]; then
	kill -9 -1
else
	echo "wrong choice" >> /dev/null;
fi
