#!/bin/bash

case $BLOCK_BUTTON in
	1) termite -e "atop";;
esac

free -h | awk '/^Mem:/ {print $3 "/" $2}'
