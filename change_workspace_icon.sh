#!/bin/bash

i3_file="$HOME/.config/i3/config"

firefox_logo=""
terminal_logo=""
spotify_logo=""
code_logo=""



browser_name="firefox"
music_player_name="spotify"
terminal_name="st"
text_editor_1="vim"
text_editor_2="electron"
text_editor_3="code" # visual studio codeq

browser_id=$(pgrep $browser_name)
terminal_id=$(pgrep $terminal_name)
music_id=$(pgrep $music_player_name)

code_editor_id_1=$(pgrep $text_editor_1)
code_editor_id_2=$(pgrep $text_editor_2)
code_editor_id_3=$(pgrep $text_editor_3)

if [ -n "$browser_id" ]; then
	sed -i -e 's/workspace 1/workspace $ws1/g' $i3_file
else
	sed -i -e 's/workspace $ws1/workspace 1/g' $i3_file
fi

if [ -n "$browser_id" ]; then
	sed -i -e 's/workspace 2/workspace $ws2/g' $i3_file
else
	sed -i -e 's/workspace $ws2/workspace 2/g' $i3_file
fi

if [ -n "$music_id" ]; then
	sed -i -e 's/workspace 3/workspace $ws3/g' $i3_file
else
	sed -i -e 's/workspace $ws3/workspace 3/g' $i3_file
fi


if [ -n "$code_editor_id_1" ]; then
	sed -i -e 's/workspace 4/workspace $ws4/g' $i3_file
elif [ -n "code_editor_id_2" ]; then
	sed -i -e 's/workspace 4/workspace $ws4/g' $i3_file
elif [ -n "code_editor_id_3"]; then
	sed -i -e 's/workspace 4/workspace $ws4/g' $i3_file
else
	sed -i -e 's/workspace $ws4/workspace 4/g' $i3_file
fi
