#!/bin/bash

sensors_data=$(sensors);
gpu_temp=$(echo $sensors_data | awk '{print $6}')
cpu_temp=$(echo $sensors_data | awk '{print $18}')
echo GPU: $gpu_temp CPU: $cpu_temp
